#!/bin/bash

set -e
set -x

KERNEL_REPO_URL="https://gitlab.com/omos/linux-public.git"
KERNEL_REPO_PATH="cache/linux-public"

RPM_REPO_URL="https://src.fedoraproject.org/rpms/kernel.git"
RPM_REPO_PATH="cache/kernel"

TEST_PATCH_NAME='test-patch.patch'
SED_SCRIPT_ADD_PATCH='s/^\(# END OF PATCH DEFINITIONS\)$/Patch900: '"$TEST_PATCH_NAME"'\n\n\1/g'
SED_SCRIPT_CONFIG='s/^\(\.\/process_configs\.sh\) \$OPTS/\1/g'
SED_SCRIPT_NO_DEBUG='s/^%define debugbuildsenabled 1$/%define debugbuildsenabled 0/g'

cat >/etc/krb5.conf.d/fedoraproject_org <<EOF
[realms]
 FEDORAPROJECT.ORG = {
        kdc = https://id.fedoraproject.org/KdcProxy
 }
[domain_realm]
 .fedoraproject.org = FEDORAPROJECT.ORG
 fedoraproject.org = FEDORAPROJECT.ORG
EOF

kinit "$KRB_USERNAME@FEDORAPROJECT.ORG" <<EOF
$KRB_PASSWORD
EOF

[ -d "$KERNEL_REPO_PATH" ] || git clone -n "$KERNEL_REPO_URL" "$KERNEL_REPO_PATH"
[ -d "$RPM_REPO_PATH"    ] || git clone -n "$RPM_REPO_URL"    "$RPM_REPO_PATH"

(cd "$KERNEL_REPO_PATH" && git tag | xargs -n 1 git tag --delete && git fetch origin --tags)
(cd "$RPM_REPO_PATH" && git fetch origin)

function parse_commitish() {
	(cd "$KERNEL_REPO_PATH" && git rev-parse --verify "$1") || \
	(cd "$KERNEL_REPO_PATH" && git rev-parse --verify "origin/$1")
}

function process_tag() {
	local tag="$1"
	local commit="$2"

	[ -d "cache/tags/$tag" ] && return 0

	local name="$(cut -d / -f 2 <<<"$tag")"
	local base="$(cut -d / -f 3 <<<"$tag")"
	local dist="$(cut -d / -f 4 <<<"$tag")"

	local commit_head="$(parse_commitish "$tag")"
	local commit_base="$(parse_commitish "$base")"

	[ -n "$commit_head" ] && [ -n "$commit_base" ]

	(
		cd "$KERNEL_REPO_PATH"
		git format-patch --stdout "$commit_base".."$commit_head" --
	) >"$RPM_REPO_PATH/$TEST_PATCH_NAME" || return 0

	(
		cd "$RPM_REPO_PATH"
		git checkout --detach
		git branch -f "$dist" "origin/$dist"
		git checkout "$dist"

		cat \
			<(echo "%global buildid .$name") \
			<(sed -e "$SED_SCRIPT_ADD_PATCH" \
				-e "$SED_SCRIPT_CONFIG" \
				-e "$SED_SCRIPT_NO_DEBUG" kernel.spec) \
			>kernel.spec.new

		mv kernel.spec.new kernel.spec

		rm -f *.src.rpm
		fedpkg srpm
		fedpkg scratch-build --nowait --srpm *.src.rpm
		git checkout '*'
	)

	mkdir -p "cache/tags/$tag"
}

(cd "$KERNEL_REPO_PATH" && git tag -l --format '%(refname:strip=2) %(objectname)' 'koji/*') | \
	while read LINE; do process_tag $LINE; done
